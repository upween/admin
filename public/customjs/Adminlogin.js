
createcaptchaheader();

function ValidateLoginHome() {
  var path = serverpath + "validatelogin/"+jQuery('#txtlogin').val()+"/"+jQuery('#txtpassword').val()
  ajaxget(path,'parsedatalogin','comment');
}   
function parsedatalogin(data){   
  data = JSON.parse(data)
  if (data.result.userDetails.ReturnValue == "1") {
       jQuery('#txtlogin').val("");
       jQuery('#txtlogin').val("");
       jQuery('#cpatchaTextBoxheader').val("");
      sessionStorage.setItem("token", data.result.token);
      sessionStorage.setItem("refreshToken", data.result.refreshToken)
      sessionStorage.setItem("RegistrationId", data.result.userDetails.RegistrationId);
      sessionStorage.setItem("CandidateId", data.result.userDetails.CandidateId);
      sessionStorage.setItem("CandidateName", data.result.userDetails.CandidateName);
      sessionStorage.setItem("MobileNumber", data.result.userDetails.MobileNumber);
      sessionStorage.setItem("EmailId", data.result.userDetails.EmailId);
      //RoleMenuMapping(data[0].RoleId);
      window.location="/";
  }
 
  else if(data.result.userDetails.ReturnValue == "2"){
      toastr.warning("Invalid UserId or Password", "", "info")
      createcaptchaheader();
      jQuery('#cpatchaTextBoxheader').val('');
      return true;
      
  }
  else{
    toastr.warning(data, "", "info")
  }
}
function CheckValidationHome(){

    if (jQuery('#txtlogin').val() == '')
    {
     
      jQuery('#txtlogin').css('border-color', 'red'); 
      return false; 
  }
  else { 
      jQuery('#txtlogin').css('border-color', '');
   }
   
    if (jQuery('#txtpassword').val() == '') {
      
      jQuery('#txtpassword').css('border-color', 'red'); 
      return false; 
  }
  else { 
      jQuery('#txtpassword').css('border-color', '');
   }
   if (jQuery('#cpatchaTextBoxheader').val() == '') {
        
      jQuery('#cpatchaTextBoxheader').css('border-color', 'red'); 
      return false; 
  }
  else { 
      jQuery('#cpatchaTextBoxheader').css('border-color', '');
      validateCaptchaheader() ;
   }
        
       
  
}
var codeheader;
                function createcaptchaheader() {
                  //clear the contents of captcha div first 
                  document.getElementById('captchaheader').innerHTML = "";
                  var charsArray =
                  "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ@!#$%^&*";
                  var lengthOtp = 6;
                  var captcha = [];
                  for (var i = 0; i < lengthOtp; i++) {
                    //below code will not allow Repetition of Characters
                    var index = Math.floor(Math.random() * charsArray.length + 1); //get the next character from the array
                    if (captcha.indexOf(charsArray[index]) == -1)
                      captcha.push(charsArray[index]);
                    else i--;
                  }
                  var canv = document.createElement("canvas");
                  canv.id = "captchaheader";
                  canv.width = 100;
                  canv.height = 50;
                  var ctx = canv.getContext("2d");
                  ctx.font = "25px Georgia";
                  ctx.strokeText(captcha.join(""), 0, 30);
                  //storing captcha so that can validate you can save it somewhere else according to your specific requirements
                  codeheader = captcha.join("");
                  document.getElementById("captchaheader").appendChild(canv); // adds the canvas to the body element
                }
                function validateCaptchaheader() {
                  event.preventDefault();
                  
                  if (document.getElementById("cpatchaTextBoxheader").value == codeheader) {
                    ValidateLoginHome() ;
                    
                  }else{
                    jQuery('#cpatchaTextBoxheader').val("");
                    toastr.warning("Invalid Captcha", "", "info");
                    createcaptchaheader();
                    jQuery('#cpatchaTextBoxheader').css('border-color', 'red'); 
                    return true;
                    
                  }
                }
    

                function checkpassword(){
                  var txtpassword;
                  txtpassword = jQuery('#txtpassword').val();
                  var reg = /[0-9a-fA-F]{4,8}/;
                  if (reg.test(txtpassword) == false) {
                      $("#txtpassword").focus();
                      toastr.warning("Please Enter Correct Password", "", "info")
                      return true;
                  }
              }