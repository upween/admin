
function FillMenu(funct,control){
	var path = serverpath + "secured/menu/0/Top/0/0/0"
	securedajaxget(path,funct,'comment',control);
	}
	function parsedatasecuredMenu(data,control){
	data = JSON.parse(data)
	if (data.message == "New token generated"){
	sessionStorage.setItem("token", data.data.token);
	FillMenu('parsedatasecuredMenu',control);
	}
	else{
	var data1 = data[0];
	jQuery("#"+control).empty();
	jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("No Parent"));
	for (var i = 0; i < data1.length; i++) {
	jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].MenuId).html(data1[i].MenuName));
	}
}
}
function CheckValidation(){
    if (jQuery('#txtMenuName').val() == '') {
        jQuery('#m_scroll_top').click();
        swal("Please Enter Menu Name", "", "info");
    }

    else if (jQuery('#Link').val() == '') {
        jQuery('#m_scroll_top').click();
        swal("Please Enter Link", "", "info");
    }
    else if (jQuery('#ddlParentId').val() == '') {
        jQuery('#m_scroll_top').click();
        swal("Please Select ParentId", "", "info");
    }
    else if (jQuery('#ddlPriority').val() == '0') {
        jQuery('#m_scroll_top').click();
        swal("Please Select Priority", "", "info");
    }
		else{
			Insupdmenu();
		}      
};
function Insupdmenu(){
	var chkval = $("#chkActiveStatus")[0].checked
	if (chkval == true){
		chkval = "1"
	}else{
		chkval="0"
	}
var MasterData = {
	"p_menuid": localStorage.MenuId,
    "p_menuname":jQuery("#txtMenuName").val(),
    "p_parentid":jQuery("#ddlParentId option:selected").val(),
    "p_priority":jQuery("#ddlPriority option:selected").val(),
    "p_LinkURL":jQuery("#Link").val(),
    "p_isactive":chkval,
    "p_MWB":"Top",
    "p_IpAddress": sessionStorage.getItem("IpAddress"),
    "p_UserId": "1"

};
MasterData = JSON.stringify(MasterData)
var path = serverpath + "secured/menu";
securedajaxpost(path, 'parsrdataitmenu', 'comment', MasterData, 'control')
}


function parsrdataitmenu(data) {
	data = JSON.parse(data)
	if (data.message == "New token generated"){
			sessionStorage.setItem("token", data.data.token);
			Insupdmenu();
	}
	else if (data[0][0].ReturnValue == "1") {
		resetMode();
		fillmenu('parsedatasecuredfillFetchmenu','tbodyvalue');
		 toastr.success("Insert Successful", "", "success")
			return true;
	}
	else if (data[0][0].ReturnValue == "2") {
		resetMode();
		fillmenu('parsedatasecuredfillFetchmenu','tbodyvalue');
			toastr.success("Update Successful", "", "success")
			return true;
	}
	else if (data[0][0].ReturnValue == "0") {
		resetMode();
		fillmenu('parsedatasecuredfillFetchmenu','tbodyvalue');
		toastr.success("already exist", "", "info")
			return true;
	}
 
}

function resetMode() {
	$("#btnSubmit").html('Submit');
	localStorage.MenuId = "0";
		jQuery("#txtMenuName").val(""); 
		jQuery("#Link").val(""); 
		$("#ddlParentId").val("0"); 
		$("#ddlPriority").val("0"); 
		$("#chkActiveStatus").prop("checked", false);
	}

	function fillmenu(funct,control) {
    var path =  serverpath + "secured/menu/0/Top/0/0/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredfillFetchmenu(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        fillmenu('parsedatasecuredfillFetchmenu','tbodyvalue');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
    else{
        var data1 = data[0];
        var appenddata="";
        jQuery("#"+control).empty();
        for (var i = 0; i < data[0].length; i++) {
			if (data1[i].IsActive == '1'){
            appenddata += "<tr><td>"+ data1[i].MenuId +" </td><td>"+ data1[i].MenuName +"</td><td>"+ data1[i].ParentId +"</td><td>"+ data1[i].Priority +"</td><td>Y</td><td><span style='margin-left:10px;'><a href='#' onclick=editMode('"+data[0][i].MenuId+"','"+encodeURI(data[0][i].MenuName)+"','"+data[0][i].LinkURL+"','"+data[0][i].ParentId+"','"+data[0][i].Priority+"','"+data[0][i].IsActive+"')><i class='fa fa-pencil'></i></a></span></td> </tr> ";
		   }
		 else  if (data1[i].IsActive == '0'){
            appenddata += "<tr><td>"+ data1[i].MenuId +" </td><td>"+ data1[i].MenuName +"</td><td>"+ data1[i].ParentId +"</td><td>"+ data1[i].Priority +"</td><td>N</td><td><span style='margin-left:10px;'><a href='#' onclick=editMode('"+data[0][i].MenuId+"','"+encodeURI(data[0][i].MenuName)+"','"+data[0][i].LinkURL+"','"+data[0][i].ParentId+"','"+data[0][i].Priority+"','"+data[0][i].IsActive+"')><i class='fa fa-pencil'></i></a></span></td> </tr> ";
		   }
		   }
       jQuery("#"+control).html(appenddata);
    }
}


function editMode(MenuId, MenuName,LinkURL,ParentId, Priority,IsActive ) 
{
	jQuery('#m_scroll_top').click();
	localStorage.MenuId = MenuId;
	$("#btnSubmit").html('Update');
	if (IsActive == "1") {
			$("#chkActiveStatus").prop("checked", true);
	} else {
			$("#chkActiveStatus").prop("checked", false);
	}

    jQuery("#txtMenuName").val(decodeURI(MenuName));
    jQuery("#ddlParentId").val(ParentId);
    jQuery("#ddlPriority").val(Priority);
    jQuery("#Link").val(LinkURL);


}