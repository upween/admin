function CheckValidation(){
    if (jQuery('#txtIndustry').val() == '') {
        jQuery('#m_scroll_top').click();
        swal("Please Select Industry Name", "", "info");       
    }
		else{
			InsupdIndustry();
		}      
};
function InsupdIndustry(){
	var chkval = $("#chkActiveStatus")[0].checked
	if (chkval == true){
		chkval = "1"
	}else{
		chkval="0"
	}
var MasterData = {
	"p_IndustryId": localStorage.IndustryId,
	"p_IndustryName":jQuery("#txtIndustry").val(),
	"p_UserId": "1",
	"p_IsActive":chkval,
	"p_IpAddress": sessionStorage.getItem("IpAddress")
};
MasterData = JSON.stringify(MasterData)
var path = serverpath + "secured/industry";
securedajaxpost(path, 'parsrdataitIndustry', 'comment', MasterData, 'control')
}


function parsrdataitIndustry(data) {
	data = JSON.parse(data)
	if (data.message == "New token generated"){
			sessionStorage.setItem("token", data.data.token);
			InsupdIndustry();
	}
	else if (data[0][0].ReturnValue == "1") {
		resetMode();
        fillIndustry('parsedatasecuredfillFetchIndustry','tbodyvalue');
		 toastr.success("Insert Successful", "", "success")
			return true;
	}
	else if (data[0][0].ReturnValue == "2") {
		resetMode();
        fillIndustry('parsedatasecuredfillFetchIndustry','tbodyvalue');
			toastr.success("Update Successful", "", "success")
			return true;
	}
	else if (data[0][0].ReturnValue == "0") {
		resetMode();
        fillIndustry('parsedatasecuredfillFetchIndustry','tbodyvalue');
		toastr.success("already exist", "", "info")
			return true;
	}
 
}

function resetMode() {
	$("#btnSubmit").html('Submit');
	localStorage.IndustryId = "0";
		jQuery("#txtIndustry").val(""); 
		$("#chkActiveStatus").prop("checked", false);
	}

	function fillIndustry(funct,control) {
    var path =  serverpath + "secured/industry/0/0/0/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredfillFetchIndustry(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        fillIndustry('parsedatasecuredfillFetchIndustry','tbodyvalue');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
    else{
        var data1 = data[0];
        var appenddata="";
        jQuery("#"+control).empty();
        for (var i = 0; i < data[0].length; i++) {
			if (data1[i].IsActive == '1'){
            appenddata += "<tr><td>"+ data1[i].IndustryId +" </td><td>"+ data1[i].IndustryName +"</td><td>Y</td><td><span style='margin-left:10px;'><a href='#' onclick=editMode('"+data[0][i].IndustryId+"','"+data[0][i].IndustryName+"','"+data[0][i].IsActive+"')><i class='fa fa-pencil'></i></a></span></td> </tr> ";
		 }
		 if (data1[i].IsActive == '0'){
            appenddata += "<tr><td>"+ data1[i].IndustryId +" </td><td>"+ data1[i].IndustryName +"</td><td>N</td><td><span style='margin-left:10px;'><a href='#' onclick=editMode('"+data[0][i].IndustryId+"','"+data[0][i].IndustryName+"','"+data[0][i].IsActive+"')><i class='fa fa-pencil'></i></a></span></td> </tr> ";
		 } }
       jQuery("#"+control).html(appenddata);
    }
}


function editMode(IndustryId, IndustryName, IsActive ) 
{
	jQuery('#m_scroll_top').click();
	localStorage.IndustryId = IndustryId;
	$("#btnSubmit").html('Update');
	if (IsActive == "1") {
			$("#chkActiveStatus").prop("checked", true);
	} else {
			$("#chkActiveStatus").prop("checked", false);
	}

	jQuery("#txtIndustry").val(decodeURI(IndustryName));

}