function CheckValidation(){
	if (jQuery('#txtStateName').val() == '') {
			jQuery('#m_scroll_top').click();
			swal("Please Select state Name", "", "info");       
	}
	else{
		Insupdstate();
	}      
};
function Insupdstate(){
var chkval = $("#chkActiveStatus")[0].checked
if (chkval == true){
	chkval = "1"
}else{
	chkval="0"
}
var MasterData = {
"p_StateId":localStorage.StateId,
"p_StateName":jQuery("#txtStateName").val(),
"p_CountryId":'0',
"p_UserId": 1,
"p_IsActive":chkval,
"p_IpAddress": sessionStorage.getItem("IpAddress")
};
MasterData = JSON.stringify(MasterData)
var path = serverpath + "secured/state";
securedajaxpost(path, 'parsrdataitstate', 'comment', MasterData, 'control')
}


function parsrdataitstate(data) {
data = JSON.parse(data)
if (data.message == "New token generated"){
		sessionStorage.setItem("token", data.data.token);
		Insupdstate();
}
else if (data[0][0].ReturnValue == "1") {
	fillstate('parsedatasecuredfillFetchstate','tbodyvalue');
 resetMode();
	 toastr.success("Insert Successful", "", "success")
		return true;
}
else if (data[0][0].ReturnValue == "2") {
	fillstate('parsedatasecuredfillFetchstate','tbodyvalue');
	resetMode();
		toastr.success("Update Successful", "", "success")
		return true;
}
else if (data[0][0].ReturnValue == "0") {
	fillstate('parsedatasecuredfillFetchstate','tbodyvalue');
	resetMode();
		toastr.success("already exists", "", "success")
		return true;
}

}

function resetMode() {
	$("#btnsubmit").html('Submit');
	localStorage.StateId = "0";
	jQuery("#txtStateName").val(""); 
	$("#chkActiveStatus").prop("checked", false);
}

function fillstate(funct,control) {
	var path =  serverpath + "secured/state/0/0/0/0"
	securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredfillFetchstate(data,control){  
	data = JSON.parse(data)
	if (data.message == "New token generated"){
			sessionStorage.setItem("token", data.data.token);
			fillstate('parsedatasecuredfillFetchstate','tbodyvalue');
	}
	else if (data.status == 401){
			toastr.warning("Unauthorized", "", "info")
			return true;
	}
	else{
			var data1 = data[0];
			var appenddata="";
			jQuery("#"+control).empty();
			for (var i = 0; i < data[0].length; i++) {
				if (data1[i].IsActive == '1'){
					appenddata += "<tr><td>"+ data1[i].StateId +" </td><td>"+ data1[i].StateName +"</td><td>Y</td><td><span style='margin-left:10px;'><a href='#' onclick=editMode('"+data[0][i].StateId+"','"+encodeURI(data[0][i].StateName)+"','"+data[0][i].IsActive+"')><i class='fa fa-pencil'></i></a></span></td> </tr> ";
				}	
				if (data1[i].IsActive == '0'){
					appenddata += "<tr><td>"+ data1[i].StateId +" </td><td>"+ data1[i].StateName +"</td><td>N</td><td><span style='margin-left:10px;'><a href='#' onclick=editMode('"+data[0][i].StateId+"','"+encodeURI(data[0][i].StateName)+"','"+data[0][i].IsActive+"')><i class='fa fa-pencil'></i></a></span></td> </tr> ";
				}	}
		 jQuery("#"+control).html(appenddata);
	}
}


function editMode(StateId, StateName, IsActive ) 
{
jQuery('#m_scroll_top').click();
localStorage.StateId = StateId;
$("#btnSubmit").html('Update');
if (IsActive == "1") {
		$("#chkActiveStatus").prop("checked", true);
} else {
		$("#chkActiveStatus").prop("checked", false);
}

jQuery("#txtStateName").val(decodeURI(StateName));

}
