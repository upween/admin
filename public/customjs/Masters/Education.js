function CheckValidation(){
    if (jQuery('#txtEducationName').val() == '') {
        jQuery('#m_scroll_top').click();
        swal("Please Select Education Name", "", "info");       
    }
		else{
			InsupdEducation();
		}      
};
function InsupdEducation(){
	var chkval = $("#chkActiveStatus")[0].checked
	if (chkval == true){
		chkval = "1"
	}else{
		chkval="0"
	}
var MasterData = {
	"p_EducationId": localStorage.EducationId,
	"p_EducationName":jQuery("#txtEducationName").val(),
	"p_IsActive": chkval,
	"p_UserId":"1",
	"p_IpAddress": sessionStorage.getItem("IpAddress")
};
MasterData = JSON.stringify(MasterData)
var path = serverpath + "secured/education";
securedajaxpost(path, 'parsrdataitEducation', 'comment', MasterData, 'control')
}


function parsrdataitEducation(data) {
	data = JSON.parse(data)
	if (data.message == "New token generated"){
			sessionStorage.setItem("token", data.data.token);
			InsupdEducation();
	}
	else if (data[0][0].ReturnValue == "1") {
		resetMode();
        fillEducation('parsedatasecuredfillFetchEducation','tbodyvalue');
		 toastr.success("Insert Successful", "", "success")
			return true;
	}
	else if (data[0][0].ReturnValue == "2") {
		resetMode();
		fillEducation('parsedatasecuredfillFetchEducation','tbodyvalue');
			toastr.success("Update Successful", "", "success")
			return true;
	}
	else if (data[0][0].ReturnValue == "0") {
		resetMode();
		fillEducation('parsedatasecuredfillFetchEducation','tbodyvalue');
		toastr.success("already exist", "", "info")
			return true;
	}
 
}

function resetMode() {
	$("#btnSubmit").html('Submit');
	localStorage.EducationId = "0";
		jQuery("#txtEducationName").val(""); 
		$("#chkActiveStatus").prop("checked", false);
	}

	function fillEducation(funct,control) {
    var path =  serverpath + "secured/education/0/0/0/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredfillFetchEducation(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        fillEducation('parsedatasecuredfillFetchEducation','tbodyvalue');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
    else{
        var data1 = data[0];
        var appenddata="";
        jQuery("#"+control).empty();
		for (var i = 0; i < data[0].length; i++)
		 {
			if (data1[i].IsActive == '1'){
            appenddata += "<tr><td>"+ data1[i].EducationId +" </td><td>"+ data1[i].EducationName +"</td><td>Y</td><td><span style='margin-left:10px;'><a href='#' onclick=editMode('"+data[0][i].EducationId+"','"+data[0][i].EducationName+"','"+data[0][i].IsActive+"')><i class='fa fa-pencil'></i></a></span></td> </tr> ";
		  } 
		 else if (data1[i].IsActive == '0'){
            appenddata += "<tr><td>"+ data1[i].EducationId +" </td><td>"+ data1[i].EducationName +"</td><td>N</td><td><span style='margin-left:10px;'><a href='#' onclick=editMode('"+data[0][i].EducationId+"','"+data[0][i].EducationName+"','"+data[0][i].IsActive+"')><i class='fa fa-pencil'></i></a></span></td> </tr> ";
		  } 
		}
       jQuery("#"+control).html(appenddata);
    }
}


function editMode(EducationId, EducationName, IsActive ) 
{
	jQuery('#m_scroll_top').click();
	localStorage.EducationId = EducationId;
	$("#btnSubmit").html('Update');
	if (IsActive == "1") {
			$("#chkActiveStatus").prop("checked", true);
	} else {
			$("#chkActiveStatus").prop("checked", false);
	}

	jQuery("#txtEducationName").val(decodeURI(EducationName));

}