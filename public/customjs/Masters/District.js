function FillstateSecured(funct,control){
    var path = serverpath + "secured/state/0/0/0/0"
    securedajaxget(path,funct,'comment',control);
    }

 function parsedatasecuredstate(data,control){
    data = JSON.parse(data)
    if (data.message == "New token generated"){
    sessionStorage.setItem("token", data.data.token);
    FillstateSecured('parsedatasecuredstate',control);
    }
    else{
    var data1 = data[0];
    jQuery("#"+control).empty();
    jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select State"));
    for (var i = 0; i < data1.length; i++) {
    jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].StateId).html(data1[i].StateName));
         }
} }

function CheckValidation(){
    if (jQuery('#txtDistrictName').val() == '') {
        jQuery('#m_scroll_top').click();
        swal("Please Enter District Name", "", "info");       
    }
    else   if (jQuery('#ddlState').val() == '0') {
        jQuery('#m_scroll_top').click();
        swal("Please Select State Name", "", "info");       
    }
		else{
			Insupddistrict();
		}      
};
function Insupddistrict(){
	var chkval = $("#chkActiveStatus")[0].checked
	if (chkval == true){
		chkval = "1"
	}else{
		chkval="0"
	}
var MasterData = {
	"p_DistrictId": localStorage.DistrictId,
    "p_DistrictName":jQuery("#txtDistrictName").val(),
    "p_StateId":jQuery("#ddlState option:selected").val(),
	"p_IsActive":chkval,
    "p_IpAddress": sessionStorage.getItem("IpAddress"),
    "p_UserId": "1"
};
MasterData = JSON.stringify(MasterData)
var path = serverpath + "secured/district";
securedajaxpost(path, 'parsrdataitdistrict', 'comment', MasterData, 'control')
}


function parsrdataitdistrict(data) {
	data = JSON.parse(data)
	if (data.message == "New token generated"){
			sessionStorage.setItem("token", data.data.token);
			Insupddistrict();
	}
	else if (data[0][0].ReturnValue == "1") {
		resetMode();
        filldistrict('parsedatasecuredfillFetchdistrict','tbodyvalue');
		 toastr.success("Insert Successful", "", "success")
			return true;
	}
	else if (data[0][0].ReturnValue == "2") {
		resetMode();
        filldistrict('parsedatasecuredfillFetchdistrict','tbodyvalue');
			toastr.success("Update Successful", "", "success")
			return true;
    }
    else if (data[0][0].ReturnValue == "0") {
		resetMode();
        filldistrict('parsedatasecuredfillFetchdistrict','tbodyvalue');
			toastr.success("already exist", "", "info")
			return true;
	}
 
}

function resetMode() {
	$("#btnSubmit").html('Submit');
	localStorage.DistrictId = "0";
		jQuery("#txtDistrictName").val(""); 
        $("#chkActiveStatus").prop("checked", false);
        $("#ddlState").val("0");
	}

	function filldistrict(funct,control) {
    var path =  serverpath + "secured/district/0/0/0/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredfillFetchdistrict(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        filldistrict('parsedatasecuredfillFetchdistrict','tbodyvalue');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
    else{
        var data1 = data[0];
        var appenddata="";
        jQuery("#"+control).empty();
        for (var i = 0; i < data[0].length; i++) {
            if (data1[i].IsActive == '1'){
            appenddata += "<tr><td>"+ data1[i].DistrictId +" </td><td>"+ data1[i].DistrictName +"</td><td>"+ data1[i].StateName +"</td><td>Y</td><td><span style='margin-left:10px;'><a href='#' onclick=editMode('"+data[0][i].DistrictId+"','"+encodeURI(data[0][i].DistrictName)+"','"+encodeURI(data[0][i].StateId)+"','"+data[0][i].IsActive+"')><i class='fa fa-pencil'></i></a></span></td> </tr> ";
         } 
       else  if (data1[i].IsActive == '0'){
            appenddata += "<tr><td>"+ data1[i].DistrictId +" </td><td>"+ data1[i].DistrictName +"</td><td>"+ data1[i].StateName +"</td><td>N</td><td><span style='margin-left:10px;'><a href='#' onclick=editMode('"+data[0][i].DistrictId+"','"+encodeURI(data[0][i].DistrictName)+"','"+encodeURI(data[0][i].StateId)+"','"+data[0][i].IsActive+"')><i class='fa fa-pencil'></i></a></span></td> </tr> ";
         } 
        }
       jQuery("#"+control).html(appenddata);
    }
}


function editMode(DistrictId, DistrictName,StateId, IsActive ) 
{
	jQuery('#m_scroll_top').click();
	localStorage.DistrictId = DistrictId;
	$("#btnSubmit").html('Update');
	if (IsActive == "1") {
			$("#chkActiveStatus").prop("checked", true);
	} else {
			$("#chkActiveStatus").prop("checked", false);
	}

	jQuery("#txtDistrictName").val(decodeURI(DistrictName));
       $('#ddlState').val(decodeURI(StateId));
}