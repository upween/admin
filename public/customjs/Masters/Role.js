function CheckValidation(){
    if (jQuery('#txtRoleName').val() == '') {
        jQuery('#m_scroll_top').click();
        swal("Please Select Category Name", "", "info");       
    }
		else{
			InsupdRole();
		}      
};
function InsupdRole(){
	var chkval = $("#chkActiveStatus")[0].checked
	if (chkval == true){
		chkval = "1"
	}else{
		chkval="0"
	}
var MasterData = {
	"p_RoleId": localStorage.RoleId,
	"p_RoleName":jQuery("#txtRoleName").val(),
	"p_IsActive":chkval,
	"p_IpAddress": sessionStorage.getItem("IpAddress"),
	"p_UserId": "1"
};
MasterData = JSON.stringify(MasterData)
var path = serverpath + "secured/role";
securedajaxpost(path, 'parsrdataitRole', 'comment', MasterData, 'control')
}


function parsrdataitRole(data) {
	data = JSON.parse(data)
	if (data.message == "New token generated"){
			sessionStorage.setItem("token", data.data.token);
			InsupdCategory();
	}
	else if (data[0][0].ReturnValue == "1") {
		resetMode();
        fillRole('parsedatasecuredfillFetchRole','tbodyvalue');
		 toastr.success("Insert Successful", "", "success")
			return true;
	}
	else if (data[0][0].ReturnValue == "2") {
		resetMode();
        fillRole('parsedatasecuredfillFetchRole','tbodyvalue');
			toastr.success("Update Successful", "", "success")
			return true;
	}
	else if (data[0][0].ReturnValue == "0") {
		resetMode();
        fillRole('parsedatasecuredfillFetchRole','tbodyvalue');
		toastr.success("already exist", "", "info")
			return true;
	}
 
}

function resetMode() {
	$("#btnSubmit").html('Submit');
	localStorage.RoleId = "0";
		jQuery("#txtRoleName").val(""); 
		$("#chkActiveStatus").prop("checked", false);
	}

	function fillRole(funct,control) {
    var path =  serverpath + "secured/Role/0/0/0/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredfillFetchRole(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        fillRole('parsedatasecuredfillFetchRole','tbodyvalue');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
    else{
        var data1 = data[0];
        var appenddata="";
        jQuery("#"+control).empty();
        for (var i = 0; i < data[0].length; i++) {
			if (data1[i].IsActive == '1'){
            appenddata += "<tr><td>"+ data1[i].RoleId +" </td><td>"+ data1[i].RoleName +"</td><td>Y</td><td><span style='margin-left:10px;'><a href='#' onclick=editMode('"+data[0][i].RoleId+"','"+data[0][i].RoleName+"','"+data[0][i].IsActive+"')><i class='fa fa-pencil'></i></a></span></td> </tr> ";
		 }
		 if (data1[i].IsActive == '0'){
            appenddata += "<tr><td>"+ data1[i].RoleId +" </td><td>"+ data1[i].RoleName +"</td><td>N</td><td><span style='margin-left:10px;'><a href='#' onclick=editMode('"+data[0][i].RoleId+"','"+data[0][i].RoleName+"','"+data[0][i].IsActive+"')><i class='fa fa-pencil'></i></a></span></td> </tr> ";
		 } }
       jQuery("#"+control).html(appenddata);
    }
}


function editMode(RoleId, RoleName, IsActive ) 
{
	jQuery('#m_scroll_top').click();
	localStorage.RoleId = RoleId;
	$("#btnSubmit").html('Update');
	if (IsActive == "1") {
			$("#chkActiveStatus").prop("checked", true);
	} else {
			$("#chkActiveStatus").prop("checked", false);
	}

	jQuery("#txtRoleName").val(decodeURI(RoleName));

}