function FilleducationSecured(funct,control){
	var path = serverpath + "secured/education/0/0/0/0"
	securedajaxget(path,funct,'comment',control);
	}
	function parsedatasecurededucation(data,control){
	data = JSON.parse(data)
	if (data.message == "New token generated"){
	sessionStorage.setItem("token", data.data.token);
	FilleducationSecured('parsedatasecurededucation',control);
	}
	else{
	var data1 = data[0];
	jQuery("#"+control).empty();
	jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Education"));
	for (var i = 0; i < data1.length; i++) {
	jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].EducationId).html(data1[i].EducationName));
	  }
	}
}

function CheckValidation(){
    if (jQuery('#txtCourseName').val() == '') {
        jQuery('#m_scroll_top').click();
        swal("Please Enter Course Name", "", "info");       
    }
    else   if (jQuery('#ddlEducation').val() == '0') {
        jQuery('#m_scroll_top').click();
        swal("Please Select Education Name", "", "info");       
    }
        else{
            InsupdCourse();
        }      
};
function InsupdCourse(){
    var chkval = $("#chkActiveStatus")[0].checked
    if (chkval == true){
        chkval = "1"
    }else{
        chkval="0"
    }
var MasterData = {
    "p_CourseId": localStorage.CourseId,
    "p_CourseName":jQuery("#txtCourseName").val(),
    "p_IsActive":chkval,
    "p_EducationId":jQuery("#ddlEducation option:selected").val(),
    "p_IpAddress": sessionStorage.getItem("IpAddress"),
    "p_UserId": "1"
};
MasterData = JSON.stringify(MasterData)
var path = serverpath + "secured/course";
securedajaxpost(path, 'parsrdatacourse', 'comment', MasterData, 'control')
}
function parsrdatacourse(data) {
    data = JSON.parse(data)
    if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            InsupdCourse();
    }
    else if (data[0][0].ReturnValue == "1") {
        resetMode();
        fillcourse('parsedatasecuredfillFetchcourse','tbodyvalue');
         toastr.success("Insert Successful", "", "success")
            return true;
    }
    else if (data[0][0].ReturnValue == "2") {
        resetMode();
        fillcourse('parsedatasecuredfillFetchcourse','tbodyvalue');
            toastr.success("Update Successful", "", "success")
            return true;
    }
    else if (data[0][0].ReturnValue == "0") {
        resetMode();
        fillcourse('parsedatasecuredfillFetchcourse','tbodyvalue');
            toastr.success("already exist", "", "info")
            return true;
    }
 
}

function resetMode() {
    $("#btnSubmit").html('Submit');
    localStorage.CourseId = "0";
    $("#chkActiveStatus").prop("checked", false);
        jQuery("#txtCourseName").val("")
        $("#ddlEducation").val("0");
 }

    function fillcourse(funct,control) {
        var path =  serverpath + "secured/course/0/0/0/0/0"
        securedajaxget(path,funct,'comment',control);
    }
    function parsedatasecuredfillFetchcourse(data,control){  
        data = JSON.parse(data)
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            fillcourse('parsedatasecuredfillFetchcourse','tbodyvalue');
        }
        else if (data.status == 401){
            toastr.warning("Unauthorized", "", "info")
            return true;
        }
        else{
            var data1 = data[0];
            var appenddata="";
            jQuery("#"+control).empty();
            for (var i = 0; i < data[0].length; i++) {
                if (data1[i].IsActive == '1'){
                appenddata += "<tr><td>"+ data1[i].CourseId +" </td><td>"+ data1[i].CourseName +"</td><td>"+ data1[i].EducationName +"</td><td>Y</td><td><span style='margin-left:10px;'><a href='#' onclick=editMode('"+data[0][i].CourseId+"','"+encodeURI(data[0][i].CourseName)+"','"+data[0][i].IsActive+"','"+encodeURI(data[0][i].EducationId)+"')><i class='fa fa-pencil'></i></a></span></td> </tr> ";
            }
            else if (data1[i].IsActive == '0'){
                appenddata += "<tr><td>"+ data1[i].CourseId +" </td><td>"+ data1[i].CourseName +"</td><td>"+ data1[i].EducationName +"</td><td>N</td><td><span style='margin-left:10px;'><a href='#' onclick=editMode('"+data[0][i].CourseId+"','"+encodeURI(data[0][i].CourseName)+"','"+data[0][i].IsActive+"','"+encodeURI(data[0][i].EducationId)+"')><i class='fa fa-pencil'></i></a></span></td> </tr> ";
            }
        }
           jQuery("#"+control).html(appenddata);
        }
    }

    
function editMode(CourseId, CourseName,IsActive,EducationId ) 
{
jQuery('#m_scroll_top').click();
localStorage.CourseId = CourseId;
$("#btnSubmit").html('Update');
if (IsActive == "1") {
        $("#chkActiveStatus").prop("checked", true);
} else {
        $("#chkActiveStatus").prop("checked", false);
}

jQuery("#txtCourseName").val(decodeURI(CourseName));
   $('#ddlEducation').val(decodeURI(EducationId));
}
    