function CheckValidation() {
	if (jQuery('#txtCategoryName').val() == '') {
		jQuery('#m_scroll_top').click();
		swal("Please Select Category Name", "", "info");
	}
	else {
		InsupdCategory();
	}
};
function InsupdCategory() {
	var chkval = $("#chkActiveStatus")[0].checked
	if (chkval == true) {
		chkval = "1"
	} else {
		chkval = "0"
	}
	var MasterData = {
		"p_categoryid": localStorage.CategoryId,
		"p_categoryname": jQuery("#txtCategoryName").val(),
		"p_UserId": "1",
		"p_isactive": chkval,
		"p_IpAddress": sessionStorage.getItem("IpAddress")
	};
	MasterData = JSON.stringify(MasterData)
	var path = serverpath + "secured/category";
	securedajaxpost(path, 'parsrdataitcategory', 'comment', MasterData, 'control')
}


function parsrdataitcategory(data) {
	data = JSON.parse(data)
	if (data.message == "New token generated") {
		sessionStorage.setItem("token", data.data.token);
		InsupdCategory();
	}
	else if (data[0][0].ReturnValue == "1") {
		resetMode();
		fillCategory('parsedatasecuredfillFetchcategory', 'tbodyvalue');
		toastr.success("Insert Successful", "", "success")
		return true;
	}
	else if (data[0][0].ReturnValue == "2") {
		resetMode();
		fillCategory('parsedatasecuredfillFetchcategory', 'tbodyvalue');
		toastr.success("Update Successful", "", "success")
		return true;
	}
	else if (data[0][0].ReturnValue == "0") {
		resetMode();
		fillCategory('parsedatasecuredfillFetchcategory', 'tbodyvalue');
		toastr.success("already exist", "", "info")
		return true;
	}

}

function resetMode() {
	$("#btnSubmit").html('Submit');
	localStorage.CategoryId = "0";
	jQuery("#txtCategoryName").val("");
	$("#chkActiveStatus").prop("checked", false);
}

function fillCategory(funct, control) {
	var path = serverpath + "secured/category/0/0/0/0"
	securedajaxget(path, funct, 'comment', control);
}

function parsedatasecuredfillFetchcategory(data, control) {
	data = JSON.parse(data)
	if (data.message == "New token generated") {
		sessionStorage.setItem("token", data.data.token);
		fillCategory('parsedatasecuredfillFetchcategory', 'tbodyvalue');
	}
	else if (data.status == 401) {
		toastr.warning("Unauthorized", "", "info")
		return true;
	}
	else {
		var data1 = data[0];
		var appenddata = "";
		jQuery("#" + control).empty();

		for (var i = 0; i < data[0].length; i++) {
			if (data1[i].IsActive == '1') {
				appenddata += "<tr><td>" + data1[i].CategoryId + " </td><td>" + data1[i].CategoryName + "</td><td>Y</td><td><span style='margin-left:10px;'><a href='#' onclick=editMode('" + data[0][i].CategoryId + "','" + data[0][i].CategoryName + "','" + data[0][i].IsActive + "')><i class='fa fa-pencil'></i></a></span></td> </tr> ";
			}
			else if (data1[i].IsActive == '0') {
				appenddata += "<tr><td>" + data1[i].CategoryId + " </td><td>" + data1[i].CategoryName + "</td><td>N</td><td><span style='margin-left:10px;'><a href='#' onclick=editMode('" + data[0][i].CategoryId + "','" + data[0][i].CategoryName + "','" + data[0][i].IsActive + "')><i class='fa fa-pencil'></i></a></span></td> </tr> ";
			}
		}
		jQuery("#" + control).html(appenddata);
	}
}


function editMode(CategoryId, CategoryName, IsActive) {
	jQuery('#m_scroll_top').click();
	localStorage.CategoryId = CategoryId;
	$("#btnSubmit").html('Update');
	if (IsActive == "1") {
		$("#chkActiveStatus").prop("checked", true);
	} else {
		$("#chkActiveStatus").prop("checked", false);
	}
	jQuery("#txtCategoryName").val(decodeURI(CategoryName));

}