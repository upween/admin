function CheckValidation(){
    if (jQuery('#txtSkillName').val() == '') {
        jQuery('#m_scroll_top').click();
        swal("Please Select Skill Name", "", "info");       
    }
		else{
			Insupdskill();
		}      
};
function Insupdskill(){
	var chkval = $("#chkActiveStatus")[0].checked
	if (chkval == true){
		chkval = "1"
	}else{
		chkval="0"
	}
var MasterData = {
	"p_skillid": localStorage.SkillId,
	"p_skillname":jQuery("#txtSkillName").val(),
	"p_UserId": "1",
	"p_isactive":chkval,
	"p_IpAddress": sessionStorage.getItem("IpAddress")
};
MasterData = JSON.stringify(MasterData)
var path = serverpath + "secured/skill";
securedajaxpost(path, 'parsrdataitskill', 'comment', MasterData, 'control')
}


function parsrdataitskill(data) {
	data = JSON.parse(data)
	if (data.message == "New token generated"){
			sessionStorage.setItem("token", data.data.token);
			Insupdskill();
	}
	else if (data[0][0].ReturnValue == "1") {
		resetMode();
        fillskill('parsedatasecuredfillFetchskill','tbodyvalue');
		 toastr.success("Insert Successful", "", "success")
			return true;
	}
	else if (data[0][0].ReturnValue == "2") {
		resetMode();
        fillskill('parsedatasecuredfillFetchskill','tbodyvalue');
			toastr.success("Update Successful", "", "success")
			return true;
	}
	else if (data[0][0].ReturnValue == "0") {
		resetMode();
        fillskill('parsedatasecuredfillFetchskill','tbodyvalue');
		toastr.success("already exist", "", "info")
			return true;
	}
 
}

function resetMode() {
	$("#btnSubmit").html('Submit');
	localStorage.SkillId = "0";
		jQuery("#txtSkillName").val(""); 
		$("#chkActiveStatus").prop("checked", false);
	}

	function fillskill(funct,control) {
    var path =  serverpath + "secured/skill/0/0/0/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredfillFetchskill(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        fillskill('parsedatasecuredfillFetchskill','tbodyvalue');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
    else{
        var data1 = data[0];
        var appenddata="";
        jQuery("#"+control).empty();
        for (var i = 0; i < data[0].length; i++) {
			if (data1[i].IsActive == '1'){
            appenddata += "<tr><td>"+ data1[i].SkillId +" </td><td>"+ data1[i].SkillName +"</td><td>Y</td><td><span style='margin-left:10px;'><a href='#' onclick=editMode('"+data[0][i].SkillId+"','"+data[0][i].SkillName+"','"+data[0][i].IsActive+"')><i class='fa fa-pencil'></i></a></span></td> </tr> ";
		 }
		 if (data1[i].IsActive == '0'){
            appenddata += "<tr><td>"+ data1[i].SkillId +" </td><td>"+ data1[i].SkillName +"</td><td>N</td><td><span style='margin-left:10px;'><a href='#' onclick=editMode('"+data[0][i].SkillId+"','"+data[0][i].SkillName+"','"+data[0][i].IsActive+"')><i class='fa fa-pencil'></i></a></span></td> </tr> ";
		 } }
       jQuery("#"+control).html(appenddata);
    }
}


function editMode(SkillId, SkillName, IsActive ) 
{
	jQuery('#m_scroll_top').click();
	localStorage.SkillId = SkillId;
	$("#btnSubmit").html('Update');
	if (IsActive == "1") {
			$("#chkActiveStatus").prop("checked", true);
	} else {
			$("#chkActiveStatus").prop("checked", false);
	}

	jQuery("#txtSkillName").val(decodeURI(SkillName));

}