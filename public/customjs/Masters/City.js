function FilldistrictSecured(funct,control){
	var path = serverpath + "secured/district/0/0/0/0"
	securedajaxget(path,funct,'comment',control);
	}
	function parsedatasecureddistrict(data,control){
	data = JSON.parse(data)
	if (data.message == "New token generated"){
	sessionStorage.setItem("token", data.data.token);
	FilldistrictSecured('parsedatasecureddistrict',control);
	}
	else{
	var data1 = data[0];
	jQuery("#"+control).empty();
	jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select District"));
	for (var i = 0; i < data1.length; i++) {
	jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].DistrictId).html(data1[i].DistrictName));
	}
	}
	}
	function CheckValidation(){
		if (jQuery('#txtCityName').val() == '') {
			jQuery('#m_scroll_top').click();
			swal("Please Enter City Name", "", "info");       
		}
		else   if (jQuery('#ddlDistrict').val() == '0') {
			jQuery('#m_scroll_top').click();
			swal("Please Select District Name", "", "info");       
		}
			else{
				Insupdcity();
			}      
	};
	function Insupdcity(){
		var chkval = $("#chkActiveStatus")[0].checked
		if (chkval == true){
			chkval = "1"
		}else{
			chkval="0"
		}
	var MasterData = {
		"p_CityId": localStorage.CityId,
		"p_CityName":jQuery("#txtCityName").val(),
		"p_DistrictId":jQuery("#ddlDistrict option:selected").val(),
		"p_IsActive":chkval,
		"p_IpAddress": sessionStorage.getItem("IpAddress"),
		"p_UserId": "1"
	};
	MasterData = JSON.stringify(MasterData)
	var path = serverpath + "secured/city";
	securedajaxpost(path, 'parsrdataitcity', 'comment', MasterData, 'control')
	}
	function parsrdataitcity(data) {
		data = JSON.parse(data)
		if (data.message == "New token generated"){
				sessionStorage.setItem("token", data.data.token);
				Insupdcity();
		}
		else if (data[0][0].ReturnValue == "1") {
			resetMode();
			fillcity('parsedatasecuredfillFetchcity','tbodyvalue');
			 toastr.success("Insert Successful", "", "success")
				return true;
		}
		else if (data[0][0].ReturnValue == "2") {
			resetMode();
			fillcity('parsedatasecuredfillFetchcity','tbodyvalue');
				toastr.success("Update Successful", "", "success")
				return true;
		}
		else if (data[0][0].ReturnValue == "0") {
			resetMode();
			fillcity('parsedatasecuredfillFetchcity','tbodyvalue');
				toastr.success("already exist", "", "info")
				return true;
		}
	 
	}
	function resetMode() {
		$("#btnSubmit").html('Submit');
		localStorage.CityId = "0";
		$("#chkActiveStatus").prop("checked", false);
			jQuery("#txtCityName").val("")
			$("#ddlDistrict").val("0");
		}

		function fillcity(funct,control) {
			var path =  serverpath + "secured/city/0/0/0/0"
			securedajaxget(path,funct,'comment',control);
		}
		function parsedatasecuredfillFetchcity(data,control){  
			data = JSON.parse(data)
			if (data.message == "New token generated"){
				sessionStorage.setItem("token", data.data.token);
				fillcity('parsedatasecuredfillFetchcity','tbodyvalue');
			}
			else if (data.status == 401){
				toastr.warning("Unauthorized", "", "info")
				return true;
			}
			else{
				var data1 = data[0];
				var appenddata="";
				jQuery("#"+control).empty();
				for (var i = 0; i < data[0].length; i++) {
					if (data1[i].IsActive == '1'){
					appenddata += "<tr><td>"+ data1[i].CityId +" </td><td>"+ data1[i].CityName +"</td><td>"+ data1[i].DistrictName +"</td><td>Y</td><td><span style='margin-left:10px;'><a href='#' onclick=editMode('"+data[0][i].CityId+"','"+encodeURI(data[0][i].CityName)+"','"+encodeURI(data[0][i].DistrictId)+"','"+data[0][i].IsActive+"')><i class='fa fa-pencil'></i></a></span></td> </tr> ";
				}
				else if (data1[i].IsActive == '0'){
					appenddata += "<tr><td>"+ data1[i].CityId +" </td><td>"+ data1[i].CityName +"</td><td>"+ data1[i].DistrictName +"</td><td>N</td><td><span style='margin-left:10px;'><a href='#' onclick=editMode('"+data[0][i].CityId+"','"+encodeURI(data[0][i].CityName)+"','"+encodeURI(data[0][i].DistrictId)+"','"+data[0][i].IsActive+"')><i class='fa fa-pencil'></i></a></span></td> </tr> ";
				}
			}
			   jQuery("#"+control).html(appenddata);
			}
		}

		
function editMode(CityId, CityName,DistrictId, IsActive ) 
{
	jQuery('#m_scroll_top').click();
	localStorage.CityId = CityId;
	$("#btnSubmit").html('Update');
	if (IsActive == "1") {
			$("#chkActiveStatus").prop("checked", true);
	} else {
			$("#chkActiveStatus").prop("checked", false);
	}

	jQuery("#txtCityName").val(decodeURI(CityName));
       $('#ddlDistrict').val(decodeURI(DistrictId));
}
		