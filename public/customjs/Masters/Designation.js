function CheckValidation(){
    if (jQuery('#txtDesignationName').val() == '') {
        jQuery('#m_scroll_top').click();
        swal("Please Select Category Name", "", "info");       
    }
		else{
			Insupddesignation();
		}      
};
function Insupddesignation(){
	var chkval = $("#chkActiveStatus")[0].checked
	if (chkval == true){
		chkval = "1"
	}else{
		chkval="0"
	}
var MasterData = {
	"p_designationid": localStorage.designationId,
    "p_designationname":jQuery("#txtDesignationName").val(),
    "p_DepartmentId":"0",
	"p_isactive":chkval,
    "p_IpAddress": sessionStorage.getItem("IpAddress"),
    "p_UserId": "1"
};
MasterData = JSON.stringify(MasterData)
var path = serverpath + "secured/designation";
securedajaxpost(path, 'parsrdataitdesignation', 'comment', MasterData, 'control')
}


function parsrdataitdesignation(data) {
	data = JSON.parse(data)
	if (data.message == "New token generated"){
			sessionStorage.setItem("token", data.data.token);
			Insupddesignation();
	}
	else if (data[0][0].ReturnValue == "1") {
		resetMode();
        filldesignation('parsedatasecuredfillFetchdesignation','tbodyvalue');
		 toastr.success("Insert Successful", "", "success")
			return true;
	}
	else if (data[0][0].ReturnValue == "2") {
		resetMode();
        filldesignation('parsedatasecuredfillFetchdesignation','tbodyvalue');
			toastr.success("Update Successful", "", "success")
			return true;
	}
	else if (data[0][0].ReturnValue == "0") {
		resetMode();
        filldesignation('parsedatasecuredfillFetchdesignation','tbodyvalue');
		toastr.success("already exist", "", "info")
			return true;
	}
 
}

function resetMode() {
	$("#btnSubmit").html('Submit');
	localStorage.DesignationId = "0";
		jQuery("#txtDesignationName").val(""); 
		$("#chkActiveStatus").prop("checked", false);
	}

	function filldesignation(funct,control) {
    var path =  serverpath + "secured/designation/0/0/0/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredfillFetchdesignation(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        filldesignation('parsedatasecuredfillFetchdesignation','tbodyvalue');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
    else{
        var data1 = data[0];
        var appenddata="";
        jQuery("#"+control).empty();
        for (var i = 0; i < data[0].length; i++) {
			if (data1[i].IsActive == '1'){
            appenddata += "<tr><td>"+ data1[i].DesignationId +" </td><td>"+ data1[i].DesignationName +"</td><td>Y</td><td><span style='margin-left:10px;'><a href='#' onclick=editMode('"+data[0][i].DesignationId+"','"+encodeURI(data[0][i].DesignationName)+"','"+data[0][i].IsActive+"')><i class='fa fa-pencil'></i></a></span></td> </tr> ";
		 }
		 if (data1[i].IsActive == '0'){
            appenddata += "<tr><td>"+ data1[i].DesignationId +" </td><td>"+ data1[i].DesignationName +"</td><td>N</td><td><span style='margin-left:10px;'><a href='#' onclick=editMode('"+data[0][i].DesignationId+"','"+encodeURI(data[0][i].DesignationName)+"','"+data[0][i].IsActive+"')><i class='fa fa-pencil'></i></a></span></td> </tr> ";
		 } }
       jQuery("#"+control).html(appenddata);
    }
}


function editMode(DesignationId, DesignationName, IsActive ) 
{
	jQuery('#m_scroll_top').click();
	localStorage.DesignationId = DesignationId;
	$("#btnSubmit").html('Update');
	if (IsActive == "1") {
			$("#chkActiveStatus").prop("checked", true);
	} else {
			$("#chkActiveStatus").prop("checked", false);
	}

	jQuery("#txtDesignationName").val(decodeURI(DesignationName));

}