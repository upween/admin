function FillcourseSecured(funct,control){
	var path = serverpath + "secured/course/0/0/0/0/0"
	securedajaxget(path,funct,'comment',control);
	}
	function parsedatasecuredcourse(data,control){
	data = JSON.parse(data)
	if (data.message == "New token generated"){
	sessionStorage.setItem("token", data.data.token);
	FillcourseSecured('parsedatasecuredcourse',control);
	}
	else{
	var data1 = data[0];
	jQuery("#"+control).empty();
	jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select course"));
	for (var i = 0; i < data1.length; i++) {
	jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].CourseId).html(data1[i].CourseName));
	}
    }
}


function CheckValidation(){
    if (jQuery('#txtSpecializationName').val() == '') {
        jQuery('#m_scroll_top').click();
        swal("Please Enter Specialization Name", "", "info");       
    }
    else   if (jQuery('#ddlCourse').val() == '0') {
        jQuery('#m_scroll_top').click();
        swal("Please Select Course Name", "", "info");       
    }
        else{
            InsupdSpecialization();
        }      
};
function InsupdSpecialization(){
    var chkval = $("#chkActiveStatus")[0].checked
    if (chkval == true){
        chkval = "1"
    }else{
        chkval="0"
    }
var MasterData = {
    "p_specializationid": localStorage.SpecializationId,
    "p_specializationname":jQuery("#txtSpecializationName").val(),
    "p_isactive":chkval,
    "p_courseid":jQuery("#ddlCourse option:selected").val(),
    "p_IpAddress": sessionStorage.getItem("IpAddress"),
    "p_UserId": "1"
};
MasterData = JSON.stringify(MasterData)
var path = serverpath + "secured/specialization";
securedajaxpost(path, 'parsrdataitSpecialization', 'comment', MasterData, 'control')
}
function parsrdataitSpecialization(data) {
    data = JSON.parse(data)
    if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            InsupdSpecialization();
    }
    else if (data[0][0].ReturnValue == "1") {
        resetMode();
        fillSpecialization('parsedatasecuredfillFetchSpecialization','tbodyvalue');
         toastr.success("Insert Successful", "", "success")
            return true;
    }
    else if (data[0][0].ReturnValue == "2") {
        resetMode();
        fillSpecialization('parsedatasecuredfillFetchSpecialization','tbodyvalue');
            toastr.success("Update Successful", "", "success")
            return true;
    }
    else if (data[0][0].ReturnValue == "0") {
        resetMode();
        fillSpecialization('parsedatasecuredfillFetchSpecialization','tbodyvalue');
            toastr.success("already exist", "", "info")
            return true;
    }
 
}
function resetMode() {
    $("#btnSubmit").html('Submit');
    localStorage.SpecializationId = "0";
    $("#chkActiveStatus").prop("checked", false);
        jQuery("#txtSpecializationName").val("")
        $("#ddlCourse").val("0");
    }

    function fillSpecialization(funct,control) {
        var path =  serverpath + "secured/specialization/0/0/0/0/0"
        securedajaxget(path,funct,'comment',control);
    }
    function parsedatasecuredfillFetchSpecialization(data,control){  
        data = JSON.parse(data)
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            fillSpecialization('parsedatasecuredfillFetchSpecialization','tbodyvalue');
        }
        else if (data.status == 401){
            toastr.warning("Unauthorized", "", "info")
            return true;
        }
        else{
            var data1 = data[0];
            var appenddata="";
            jQuery("#"+control).empty();
            for (var i = 0; i < data[0].length; i++) {
                if (data1[i].IsActive == '1'){
                appenddata += "<tr><td>"+ data1[i].SpecializationId +" </td><td>"+ data1[i].SpecializationName +"</td><td>"+ data1[i].CourseName +"</td><td>Y</td><td><span style='margin-left:10px;'><a href='#' onclick=editMode('"+data[0][i].SpecializationId+"','"+encodeURI(data[0][i].SpecializationName)+"','"+encodeURI(data[0][i].CourseId)+"','"+data[0][i].IsActive+"')><i class='fa fa-pencil'></i></a></span></td> </tr> ";
            }
            else if (data1[i].IsActive == '0'){
                appenddata += "<tr><td>"+ data1[i].SpecializationId +" </td><td>"+ data1[i].SpecializationName +"</td><td>"+ data1[i].CourseName +"</td><td>N</td><td><span style='margin-left:10px;'><a href='#' onclick=editMode('"+data[0][i].SpecializationId+"','"+encodeURI(data[0][i].SpecializationName)+"','"+encodeURI(data[0][i].CourseId)+"','"+data[0][i].IsActive+"')><i class='fa fa-pencil'></i></a></span></td> </tr> ";
            }
        }
           jQuery("#"+control).html(appenddata);
        }
    }

    
function editMode(SpecializationId, SpecializationName,CourseId, IsActive ) 
{
jQuery('#m_scroll_top').click();
localStorage.SpecializationId = SpecializationId;
$("#btnSubmit").html('Update');
if (IsActive == "1") {
        $("#chkActiveStatus").prop("checked", true);
} else {
        $("#chkActiveStatus").prop("checked", false);
}

jQuery("#txtSpecializationName").val(decodeURI(SpecializationName));
   $('#ddlCourse').val(decodeURI(CourseId));
}
    