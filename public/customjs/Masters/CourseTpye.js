function CheckValidation(){
    if (jQuery('#txtcoursetype').val() == '') {
        jQuery('#m_scroll_top').click();
        swal("Please Select Category Name", "", "info");       
    }
		else{
			Insupdcoursetype();
		}      
};
function Insupdcoursetype(){
	var chkval = $("#chkActiveStatus")[0].checked
	if (chkval == true){
		chkval = "1"
	}else{
		chkval="0"
	}
var MasterData = {
	"p_CourseTypeId": localStorage.CourseTypeId,
	"p_CourseTypeName":jQuery("#txtcoursetype").val(),
	"p_IsActive":chkval,
    "p_IpAddress": sessionStorage.getItem("IpAddress"),
    "p_UserId": "1"
};
MasterData = JSON.stringify(MasterData)
var path = serverpath + "secured/coursetype";
securedajaxpost(path, 'parsrdataitcoursetype', 'comment', MasterData, 'control')
}


function parsrdataitcoursetype(data) {
	data = JSON.parse(data)
	if (data.message == "New token generated"){
			sessionStorage.setItem("token", data.data.token);
			Insupdcoursetype();
	}
	else if (data[0][0].ReturnValue == "1") {
		resetMode();
        fillcoursetype('parsedatasecuredfillFetchcoursetype','tbodyvalue');
		 toastr.success("Insert Successful", "", "success")
			return true;
	}
	else if (data[0][0].ReturnValue == "2") {
		resetMode();
        fillcoursetype('parsedatasecuredfillFetchcoursetype','tbodyvalue');
			toastr.success("Update Successful", "", "success")
			return true;
	}
	else if (data[0][0].ReturnValue == "0") {
		resetMode();
        fillcoursetype('parsedatasecuredfillFetchcoursetype','tbodyvalue');
		toastr.success("already exist", "", "info")
			return true;
	}
 
}

function resetMode() {
	$("#btnSubmit").html('Submit');
	localStorage.CourseTypeId = "0";
		jQuery("#txtcoursetype").val(""); 
		$("#chkActiveStatus").prop("checked", false);
	}

	function fillcoursetype(funct,control) {
    var path =  serverpath + "secured/coursetype/0/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredfillFetchcoursetype(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        fillcoursetype('parsedatasecuredfillFetchcoursetype','tbodyvalue');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
    else{
        var data1 = data[0];
        var appenddata="";
        jQuery("#"+control).empty();
        for (var i = 0; i < data[0].length; i++) {
			if (data1[i].IsActive == '1'){
            appenddata += "<tr><td>"+ data1[i].CourseTypeId +" </td><td>"+ data1[i].CourseTypeName +"</td><td>Y</td><td><span style='margin-left:10px;'><a href='#' onclick=editMode('"+data[0][i].CourseTypeId+"','"+encodeURI(data[0][i].CourseTypeName)+"','"+data[0][i].IsActive+"')><i class='fa fa-pencil'></i></a></span></td> </tr> ";
		 }
		 if (data1[i].IsActive == '0'){
            appenddata += "<tr><td>"+ data1[i].CourseTypeId +" </td><td>"+ data1[i].CourseTypeName +"</td><td>N</td><td><span style='margin-left:10px;'><a href='#' onclick=editMode('"+data[0][i].CourseTypeId+"','"+encodeURI(data[0][i].CourseTypeName)+"','"+data[0][i].IsActive+"')><i class='fa fa-pencil'></i></a></span></td> </tr> ";
		 } }
       jQuery("#"+control).html(appenddata);
    }
}


function editMode(CourseTypeId, CourseTypeName, IsActive ) 
{
	jQuery('#m_scroll_top').click();
	localStorage.CourseTypeId = CourseTypeId;
	$("#btnSubmit").html('Update');
	if (IsActive == "1") {
			$("#chkActiveStatus").prop("checked", true);
	} else {
			$("#chkActiveStatus").prop("checked", false);
	}

	jQuery("#txtcoursetype").val(decodeURI(CourseTypeName));

}