
toastr.options = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": true,
    "progressBar": false,
    "positionClass": "toast-bottom-right",
    "preventDuplicates": true,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
};

jQuery("#txtSearch").keyup(function () {
    searchTextInTable("tbodyvalue");
})
//$(document).ajaxStart($.blockUI({ message: '<img src="images/loading.gif" style="width:50px;height:50px" />' })).ajaxStop($.unblockUI);
function searchTextInTable(tblName) {
    var tbl;
    tbl = tblName;
    jQuery("#" + tbl + " tr:has(td)").hide(); // Hide all the rows.

    var sSearchTerm = jQuery('#txtSearch').val(); //Get the search box value

    if (sSearchTerm.length == 0) //if nothing is entered then show all the rows.
    {
        jQuery("#" + tbl + " tr:has(td)").show();
        return false;
    }
    //Iterate through all the td.
    jQuery("#" + tbl + " tr:has(td)").children().each(function () {
        var cellText = jQuery(this).text().toLowerCase();
        if (cellText.indexOf(sSearchTerm.toLowerCase()) >= 0) //Check if data matches
        {
            jQuery(this).parent().show();
            return true;
        }
    });
    //e.preventDefault();
}
function TopMenus(){
	var path = serverpath + "secured/menu/0/Top/0/0/0"
	securedajaxget(path,'parsedatasecuredtopMenu','comment','control');
    }

function parsedatasecuredtopMenu(data){
    data = JSON.parse(data)
    if (data.message == "New token generated"){
    sessionStorage.setItem("token", data.data.token);
  
    }
    else{
        sessionStorage.setItem("TopMenu", JSON.stringify(data[0]));
 

        }
}


function fetchMenuTopItemsFromSession() {
    var data = sessionStorage.getItem('TopMenu');
    var data1 = sessionStorage.getItem('TopMenu');
    var i = 1;
    jQuery("#m_header_menu").append('<ul  id="ulMainTop" class="m-menu__nav  m-menu__nav--submenu-arrow"></ul>')
    jQuery.each(jQuery.parseJSON(data), function (key, value) {
        if (value.ParentId == '0') {
            if (value.LinkURL == "#") {
                jQuery("#ulMainTop").append('<li class="m-menu__item  m-menu__item--submenu m-menu__item--rel" m-menu-submenu-toggle="click" aria-haspopup="true"><a href="javascript:;" class="m-menu__link m-menu__toggle"><span class="m-menu__item-here"></span><span class="m-menu__link-text">' + value.MenuName + '</span><i class="m-menu__hor-arrow la la-angle-down"></i><i class="m-menu__ver-arrow la la-angle-right"></i></a><div class="m-menu__submenu m-menu__submenu--classic m-menu__submenu--left"> <span class="m-menu__arrow m-menu__arrow--adjust"></span><ul id=MenuHeader' + i + ' class="m-menu__subnav"></ul>');
            }
            jQuery.each(jQuery.parseJSON(data1), function (key, value1) {
                //debugger;
                if (value.MenuId == value1.ParentId)
                    jQuery("#MenuHeader" + i).append('<li class="m-menu__item " m-menu-link-redirect="1" aria-haspopup="true"><a href="' + value1.LinkURL + '" class="m-menu__link "><span class="m-menu__link-text">' + value1.MenuName + '</span></a></li>');
            });
            if (value.PageLink == "#") {
                jQuery("#MenuHeader" + i).append('</ul></div></li>');
            }
            i++;
        }
    });

}
fetchMenuTopItemsFromSession();