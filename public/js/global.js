var globalString = "This can be accessed anywhere!";  
//var serverpath = "http://182.70.254.93:303/";
//var JobseekerPath=   "http://182.70.254.93:303/";
//var EmployerPath=   "http://182.70.254.93:403/";
var serverpath=   "http://localhost:3003/";
//var JobseekerPath=   "http://localhost:4003/";
//var EmployerPath=   "http://localhost:4003/";
$.get("https://api.ipdata.co?api-key=test", function (response) {
    sessionStorage.setItem("IpAddress",response.ip);
   sessionStorage.setItem("City",response.city );
   
}, "jsonp");
ValidateLogin();
function securedajaxget(path,type,comment,control){
    jQuery.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        headers: {'authorization': sessionStorage.getItem("token"), 'refreshToken': sessionStorage.getItem("refreshToken")},
        url: path,
        cache: false,
        dataType: "json",
        success: function (successdata) {
            window[type](JSON.stringify(successdata),control);
        },
        error: function (errordata) {
            if (errordata.status == 0){
                toastr.warning("ERR_CONNECTION_REFUSED", "", "info")
                return true;
            }
            else if (errordata.status == 401){
                window[type](JSON.stringify(errordata.responseJSON),control);
                return true;
            }
            else {
                window[type](JSON.stringify(errordata),control);
                return true;
            }
        }
    });
}
function securedajaxpost(path,type,comment,masterdata,control){
    jQuery.ajax({
        url: path,
        type: "POST",
        headers: {'authorization': sessionStorage.getItem("token"), 'refreshToken': sessionStorage.getItem("refreshToken")},
        data: masterdata,
        contentType: "application/json; charset=utf-8",
        success: function (successdata, status, jqXHR) {
            window[type](JSON.stringify(successdata));
        },
        error: function (errordata) {
            if (errordata.status == 0){
                toastr.warning("ERR_CONNECTION_REFUSED", "", "info")
                return true;
            }
            else if (errordata.status == 401){
                toastr.warning("Unauthorized", "", "info")
                return true;
            }
            else{
                window[type](JSON.stringify(errordata));
            }
        }
    });
}
function ajaxget(path,type,comment){
    jQuery.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: path,
        cache: false,
        dataType: "json",
        success: function (successdata) {
            window[type](JSON.stringify(successdata));
        },
        error: function (errordata) {
            if (errordata.status == 0){
                toastr.warning("ERR_CONNECTION_REFUSED", "", "info")
                return true;
            }
            else{
                window[type](JSON.stringify(errordata));
            }
        }
    });
}
function ajaxpost(path,type,comment,masterdata,control){
    jQuery.ajax({
        url: path,
        type: "POST",
        data: masterdata,
        contentType: "application/json; charset=utf-8",
        success: function (successdata, status, jqXHR) {
            window[type](JSON.stringify(successdata));
        },
        error: function (errordata) {
            if (errordata.status == 0){
                toastr.warning("ERR_CONNECTION_REFUSED", "", "info")
                return true;
            }
            else{
                window[type](JSON.stringify(errordata));
            }
        }
    });
}




function ValidateLogin() {
    var path = serverpath + "validatelogin/1234567890/dimple@123"
    ajaxget(path,'parsedatalogin','comment' ,'control');
  }   
  function parsedatalogin(data){   
    data = JSON.parse(data)
    if (data.result.userDetails.ReturnValue == "1") {
        sessionStorage.setItem("token", data.result.token);
        sessionStorage.setItem("refreshToken", data.result.refreshToken)
        sessionStorage.setItem("RegistrationId", data.result.userDetails.RegistrationId);
        sessionStorage.setItem("CandidateId", data.result.userDetails.CandidateId);
        sessionStorage.setItem("CandidateName", data.result.userDetails.CandidateName);
        sessionStorage.setItem("MobileNumber", data.result.userDetails.MobileNumber);
        sessionStorage.setItem("EmailId", data.result.userDetails.EmailId);
        //RoleMenuMapping(data[0].RoleId);
        //window.location="/Home";
        TopMenus();
    }
    else if(data.result.userDetails.ReturnValue == "2"){
        createCaptcha();
        jQuery('#cpatchaTextBox').val(""); 
        toastr.warning("Invalid UserId or Password", "", "info")
        return true;
        
    }
    else{
      toastr.warning(data, "", "info")
    }
  };


  

  