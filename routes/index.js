// var express = require('express');
// var router = express.Router();

// /* GET home page. */
// router.get('/', function(req, res, next) {
//   res.render('index');
// });
// /* GET about page. */
// router.get('/about', function(req, res) {
//   res.render('pages/about');
// });
// module.exports = router;
module.exports = function(app) {
  app.get('/', function(req, res) {
    res.render('index');
  });
  app.get('/Category', function(req, res) {
    res.render('pages/Masters/Category');
  });
  app.get('/City', function(req, res) {
    res.render('pages/Masters/City');
  });
  app.get('/State', function(req, res) {
    res.render('pages/Masters/State');
  });
  app.get('/Course', function(req, res) {
    res.render('pages/Masters/Course');
  });
  app.get('/CourseType', function(req, res) {
    res.render('pages/Masters/CourseType');
  });
  app.get('/Currency', function(req, res) {
    res.render('pages/Masters/Currency');
  });
  app.get('/Department', function(req, res) {
    res.render('pages/Masters/Department');
  });
  app.get('/Designation', function(req, res) {
    res.render('pages/Masters/Designation');
  });
  app.get('/Education', function(req, res) {
    res.render('pages/Masters/Education');
  });
  app.get('/Industry', function(req, res) {
    res.render('pages/Masters/Industry');
  });
  app.get('/Month', function(req, res) {
    res.render('pages/Masters/Month');
  });
  app.get('/Skill', function(req, res) {
    res.render('pages/Masters/Skill');
  });
  app.get('/district', function(req, res) {
    res.render('pages/Masters/District');
  });
  app.get('/Specialization', function(req, res) {
    res.render('pages/Masters/Specialization');
  });
  app.get('/Year', function(req, res) {
    res.render('pages/Masters/Year');
  });
  app.get('/Menu', function(req, res) {
    res.render('pages/Masters/Menu');
  });
  app.get('/JobseekerEmployer', function(req, res) {
    res.render('pages/Masters/JobseekerEmployer');
  });
  app.get('/error', function(req, res) {
    res.render('pages/error');
  });
  app.get('/role', function(req, res) {
    res.render('pages/Masters/role');
  });
  app.get('/login', function(req, res) {
    res.render('pages/login');
  });
};